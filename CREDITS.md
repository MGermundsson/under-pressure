# Credits

## Developers
- **Magnus Germundsson** - Audio, Design, Programming
- **Sara Linnér** - Graphics

## Assets Used
- **[Basic Motions FREE](https://assetstore.unity.com/packages/3d/animations/basic-motions-free-154271)** - Kevin Iglesias
- **[Cartoon BigHead Starter](https://assetstore.unity.com/packages/3d/characters/cartoon-bighead-starter-15991)** - Redhead Robot
- **Earth Texture** - [JHT's Planet Pixel Emporium](http://planetpixelemporium.com/earth8081.html)
- **[Milky Way Skybox](https://assetstore.unity.com/packages/2d/textures-materials/milky-way-skybox-94001) - Adam Bielecki
- **[Snaps Prototype | Sci-Fi / Industrial](https://assetstore.unity.com/packages/3d/environments/sci-fi/snaps-prototype-sci-fi-industrial-136759)** - Asset Store Originals
- **Sound effects** - [Zapsplat](https://www.zapsplat.com)
- **[Unity Particle Pack](https://assetstore.unity.com/packages/essentials/tutorial-projects/unity-particle-pack-127325)** - Unity Technologies Inc.
