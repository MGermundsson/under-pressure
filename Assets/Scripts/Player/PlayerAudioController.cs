using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(RoomDetector))]
public class PlayerAudioController : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;

    [Header("Volume Thresholds")]

    [SerializeField] private float normalMin = 0.3f;
    [SerializeField] private float normalMax = 1f;

    [SerializeField] private float lowPressureMin = 0.7f;
    [SerializeField] private float lowPressureMax = 0.2f;

    [SerializeField] private float highPressureMin = 1f;
    [SerializeField] private float highPressureMax = 5f;


    private RoomDetector roomDetector;


    private void Start()
    {
        if (!audioMixer)
            Debug.LogError("Audio Mixer has not been set");

        roomDetector = GetComponent<RoomDetector>();
    }

    private void Update()
    {
        if (audioMixer)
        {
            Room currentRoom = roomDetector.CurrentRoom;

            if (currentRoom)
            {
                float pressure = currentRoom.Pressure;

                float normalVolume = Mathf.Log10(Mathf.InverseLerp(normalMin, normalMax, pressure)) * 20f;
                float lowPressureVolume = Mathf.Log10(Mathf.InverseLerp(lowPressureMin, lowPressureMax, pressure)) * 20f;
                float highPressureVolume = Mathf.Log10(Mathf.InverseLerp(highPressureMin, highPressureMax, pressure)) * 20f;

                audioMixer.SetFloat("NormalVolume", Mathf.Clamp(normalVolume, -80f, 0f));
                audioMixer.SetFloat("LowPressureVolume", Mathf.Clamp(lowPressureVolume, -80f, 0f));
                audioMixer.SetFloat("HighPressureVolume", Mathf.Clamp(highPressureVolume, -80f, 0f));
            }
        }
    }
}
