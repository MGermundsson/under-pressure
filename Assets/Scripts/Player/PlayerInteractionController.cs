using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionController : MonoBehaviour
{
    [SerializeField] private float radius = 1f;


    private LayerMask layerMask;

    private Interactable currentInteractable;


    public Interactable CurrentInteractable { get { return currentInteractable; } }


    private void Update()
    {
        layerMask = 1 << LayerMask.NameToLayer("Interactable");

        currentInteractable = GetClosestInteractable();

        if (currentInteractable && (Input.GetButtonDown("Interact")))
            currentInteractable.Interact();
    }

    private List<Interactable> GetInteractables()
    {
        List<Interactable> interactables = new List<Interactable>();

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layerMask);
        foreach (Collider collider in colliders)
        {
            foreach (Interactable interactable in collider.GetComponentsInChildren<Interactable>())
            {
                interactables.Add(interactable);
            }
        }

        return interactables;
    }

    private Interactable GetClosestInteractable()
    {
        Interactable closestInteractable = null;

        List<Interactable> interactables = GetInteractables();
        if (interactables.Count > 0)
        {
            closestInteractable = interactables[0];

            if (interactables.Count > 1)
            {
                float smallestDistance = Vector3.Distance(transform.position, closestInteractable.transform.position);

                for (int i = 1; i < interactables.Count; i++)
                {
                    float distance = Vector3.Distance(transform.position, interactables[i].transform.position);

                    if (distance < smallestDistance)
                    {
                        closestInteractable = interactables[i];
                        smallestDistance = distance;
                    }
                }
            }
        }

        return closestInteractable;
    }
}
