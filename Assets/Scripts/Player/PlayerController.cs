using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerHealth))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float maxVelocity = 3f;

    [SerializeField] [Range(0f, 1f)] private float velocitySmoothing = 0.1f;


    private CharacterController controller;

    private PlayerHealth playerHealth;

    private Vector3 velocity = Vector3.zero;


    public Vector3 Velocity { get { return velocity; } }
    public Vector3 NormalizedVelocity { get { return velocity / maxVelocity; } }


    private void Start()
    {
        controller = GetComponent<CharacterController>();

        playerHealth = GetComponent<PlayerHealth>();
    }

    private void FixedUpdate()
    {
        if (playerHealth.Alive)
        {
            Vector2 input = getInput() * maxVelocity;

            velocity += Physics.gravity * Time.deltaTime;

            velocity = Vector3.Lerp(velocity, new Vector3(input.x, velocity.y, input.y), 1 / velocitySmoothing * Time.deltaTime);

            controller.Move(velocity * Time.deltaTime);

            if (controller.isGrounded)
                velocity.y = 0f;
        }
    }

    private Vector2 getInput()
    {
        float xAxis = Input.GetAxisRaw("Horizontal");
        float yAxis = Input.GetAxisRaw("Vertical");

        Vector2 input = new Vector3(xAxis, yAxis);

        input = Vector2Extensions.Rotate(input, -CameraController.Instance.transform.localEulerAngles.y);

        return input;
    }
}
