using UnityEngine;

[RequireComponent(typeof(RoomDetector))]
[RequireComponent(typeof(PlayerAnimator))]
public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;

    [Space(5f)]

    [SerializeField] private float regeneration = 5f;

    [Header("High Pressure Settings")]

    [SerializeField] private float highPressureDamage = 25f;

    [Space(5f)]

    [SerializeField] [Range(1f, 10f)] private float highPressureThresholdStart = 3f;
    [SerializeField] [Range(1f, 10f)] private float highPressureThresholdEnd = 5f;

    [Header("Low Pressure Settings")]

    [SerializeField] private float lowPressureDamage = 10f;

    [Space(5f)]

    [SerializeField] [Range(0f, 1f)] private float lowPressureThresholdStart = 0.5f;
    [SerializeField] [Range(0f, 1f)] private float lowPressureThresholdEnd = 0.1f;


    private RoomDetector roomDetector;
    private PlayerAnimator playerAnimator;


    private bool alive = true;

    private float currentHealth;


    public bool Alive { get { return alive; } }

    public float CurrentHealth { get { return currentHealth; } }
    public float CurrentHealthPercentage { get { return Mathf.InverseLerp(0f, maxHealth, currentHealth); } }


    private void OnValidate()
    {
        if (maxHealth < 1f)
            maxHealth = 1f;

        if (highPressureThresholdEnd < highPressureThresholdStart)
            highPressureThresholdEnd = highPressureThresholdStart;

        if (lowPressureThresholdEnd > lowPressureThresholdStart)
            lowPressureThresholdEnd = lowPressureThresholdStart;
    }

    private void Start()
    {
        roomDetector = GetComponent<RoomDetector>();
        playerAnimator = GetComponent<PlayerAnimator>();

        currentHealth = maxHealth;
    }

    private void Update()
    {
        if (alive)
        {
            Room currentRoom = roomDetector.CurrentRoom;

            if (currentRoom)
            {
                float currentPressure = currentRoom.Pressure;

                if (currentPressure < lowPressureThresholdStart)
                {
                    // Handle low pressure calculations

                    float ratio = Mathf.InverseLerp(lowPressureThresholdStart, lowPressureThresholdEnd, currentPressure);

                    currentHealth -= lowPressureDamage * ratio * Time.deltaTime;


                    // Apply camera effects - Low pressure
                    CameraController.Instance.CurrentChromaticAbberation = ratio;
                }
                else
                    CameraController.Instance.CurrentChromaticAbberation = 0f;

                if (currentPressure > highPressureThresholdStart)
                {
                    // Handle high pressure calculations

                    float ratio = Mathf.InverseLerp(highPressureThresholdStart, highPressureThresholdEnd, currentPressure);

                    currentHealth -= highPressureDamage * ratio * Time.deltaTime;


                    // Apply camera effects - High pressure
                    CameraController.Instance.CurrentCameraShake = ratio;
                }
                else
                    CameraController.Instance.CurrentCameraShake = 0f;
            }

            // Apply camera effects - Health
            CameraController.Instance.CurrentZoom = CurrentHealthPercentage;
            CameraController.Instance.CurrentVignette = 1 - CurrentHealthPercentage;

            if (currentHealth <= 0f)
            {
                Kill();
            }
            else
            {
                currentHealth += regeneration * Time.deltaTime;

                currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);
            }
        }
    }

    public void Kill()
    {
        alive = false;
        currentHealth = 0f;

        if (playerAnimator)
            playerAnimator.Ragdoll();
    }

    public void Resurrect()
    {
        alive = true;
        currentHealth = maxHealth;

        if (playerAnimator)
            playerAnimator.UnRagdoll();
    }
}
