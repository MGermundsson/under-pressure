using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerAnimator : MonoBehaviour
{
    [SerializeField] private Transform meshRoot;

    [Space(5f)]

    [SerializeField] private Animator animator;

    [Header("Rotation Settings")]

    [SerializeField] private float smoothing = 0.25f;


    private PlayerController playerController;

    private Rigidbody[] rigidbodies;


    private bool isRagdoll = false;


    private void Start()
    {
        if (!meshRoot)
            Debug.LogError("Mesh root has not been set");

        if (!animator)
            Debug.LogError("Animator has not been set");

        playerController = GetComponent<PlayerController>();

        rigidbodies = GetComponentsInChildren<Rigidbody>();

        UnRagdoll();
    }

    private void Update()
    {
        if (!isRagdoll)
        {
            Vector3 normalizedVelocity = playerController.NormalizedVelocity;

            if (meshRoot)
            {
                Vector3 targetDirection = new Vector3(normalizedVelocity.x, 0f, normalizedVelocity.z).normalized;

                if (targetDirection != Vector3.zero)
                    meshRoot.forward = Vector3.Slerp(meshRoot.forward, targetDirection, 1 / smoothing * Time.deltaTime);
            }

            if (animator)
            {
                Vector2 animationData = new Vector2(normalizedVelocity.x, normalizedVelocity.z);

                animationData = Vector2Extensions.Rotate(animationData, meshRoot.localEulerAngles.y);

                animator.SetFloat("Horizontal", animationData.x);
                animator.SetFloat("Vertical", animationData.y);
            }
        }
    }

    public void Ragdoll()
    {
        isRagdoll = true;

        if (animator)
            animator.enabled = false;

        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = false;
        }
    }

    public void UnRagdoll()
    {
        isRagdoll = false;

        if (animator)
            animator.enabled = true;

        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.isKinematic = true;
        }
    }
}
