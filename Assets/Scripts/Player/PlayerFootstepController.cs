using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerFootstepController : MonoBehaviour
{
    [SerializeField] private float velocityThreshold = 0.1f;

    [Space(5f)]

    [SerializeField] private AudioSource normalPressureAudioSource;
    [SerializeField] private AudioSource lowPressureAudioSource;

    [Space(5f)]

    [SerializeField] private AudioClip[] normalPressureAudioClips;
    [SerializeField] private AudioClip[] lowPressureAudioClips;


    private Animator animator;


    private void Start()
    {
        if (normalPressureAudioClips.Length != lowPressureAudioClips.Length)
            Debug.LogError("Amount of audio clips does not match");

        animator = GetComponent<Animator>();
    }

    public void Step()
    {
        float horizontal = animator.GetFloat("Horizontal");
        float vertical = animator.GetFloat("Vertical");

        float velocity = (horizontal + vertical) / 2f;

        if (velocity > velocityThreshold)
        {
            int index = Random.Range(0, normalPressureAudioClips.Length);

            normalPressureAudioSource.PlayOneShot(normalPressureAudioClips[index]);
            lowPressureAudioSource.PlayOneShot(lowPressureAudioClips[index]);
        }
    }
}
