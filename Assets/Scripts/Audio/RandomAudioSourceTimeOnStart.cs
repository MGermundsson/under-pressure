using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomAudioSourceTimeOnStart : MonoBehaviour
{
    private void Start()
    {
        AudioSource audioSource = GetComponent<AudioSource>();

        audioSource.Play();
        audioSource.time = Random.Range(0f, audioSource.clip.length);

        Destroy(this);
    }
}
