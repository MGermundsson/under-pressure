using UnityEngine;

[RequireComponent(typeof(RoomDetector))]
[RequireComponent(typeof(AudioSource))]
public class VacuumAudioController : MonoBehaviour
{
    [SerializeField] [Range(0f, 1f)] private float maxVolume = 1f;


    private RoomDetector roomDetector;

    private AudioSource[] audioSources;


    private void Start()
    {
        roomDetector = GetComponent<RoomDetector>();

        audioSources = GetComponentsInChildren<AudioSource>();
    }

    private void Update()
    {
        Room currentRoom = roomDetector.CurrentRoom;

        if (currentRoom)
        {
            foreach (AudioSource audioSource in audioSources)
            {
                audioSource.volume = Mathf.Clamp01(currentRoom.Pressure) * maxVolume;
            }
        }
    }
}
