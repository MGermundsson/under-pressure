using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(PostProcessVolume))]
public class CameraController : MonoBehaviour
{
    #region Singleton pattern

    private static CameraController instance;

    public static CameraController Instance
    {
        get
        {
            if (!instance)
            {
                GameObject gameObject = new GameObject("Main Camera");

                instance = gameObject.AddComponent<CameraController>();

                // Add required components, but won't be properly configured
                gameObject.AddComponent<Camera>();
                gameObject.AddComponent<PostProcessVolume>();
            }

            return instance;
        }
    }

    #endregion


    [SerializeField] private Transform target;

    [Space(5f)]

    [SerializeField] private Vector3 offset = new Vector3(3f, 4f, -3f);
    [SerializeField] private Vector3 rotation = new Vector3(35f, -45f, 0f);

    [Space(5f)]

    [SerializeField] [Range(0f, 1f)] private float smoothing = 0.3f;

    [Header("Effects Settings")]

    [SerializeField] private float minZoom = 2.5f;
    [SerializeField] private float maxZoom = 4f;

    [Space(5f)]

    [SerializeField] private float maxCameraShake = 0.1f;

    [Space(5f)]

    [SerializeField] private float maxChromaticAbberation = 1f;

    [Space(5f)]

    [SerializeField] private float minVignette = 0.3f;
    [SerializeField] private float maxVignette = 1f;


    private Vector3 currentPosition = Vector3.zero;
    
    private Camera attachedCamera;

    private ChromaticAberration chromaticAberration;
    private Vignette vignette;

    private float currentZoom = 0f;
    private float currentCameraShake = 0f;
    private float currentChromaticAbberation = 0f;
    private float currentVignette = 0f;


    public Camera AttachedCamera { get { return attachedCamera; } }

    public Transform Target { get { return target; } set { target = value; } }

    public Vector3 TargetPosition { get { return target ? target.position + offset : transform.position; } }

    public float CurrentZoom { get { return currentZoom; } set { currentZoom = Mathf.Clamp01(value); } }
    public float CurrentCameraShake { get { return currentCameraShake; } set { currentCameraShake = Mathf.Clamp01(value); } }
    public float CurrentChromaticAbberation { get { return currentChromaticAbberation; } set { currentChromaticAbberation = Mathf.Clamp01(value); } }
    public float CurrentVignette { get { return currentVignette; } set { currentVignette = Mathf.Clamp01(value); } }


    private void OnValidate()
    {
        if (maxCameraShake < 0f)
            maxCameraShake = 0f;

        if (maxZoom < minZoom)
            maxZoom = minZoom;

        if (maxVignette < minVignette)
            maxVignette = minVignette;
    }

    private void Start()
    {
        #region Singleton sanity-check

        if (instance)
        {
            Destroy(this);
            return;
        }

        instance = this;

        #endregion

        attachedCamera = GetComponent<Camera>();

        // Get desired post-processing components
        PostProcessVolume postProcessVolume = GetComponent<PostProcessVolume>();
        postProcessVolume.profile.TryGetSettings(out chromaticAberration);
        postProcessVolume.profile.TryGetSettings(out vignette);

        ResetCamera();
    }

    private void LateUpdate()
    {
        currentPosition = Vector3.Lerp(currentPosition, TargetPosition, 1 / smoothing * Time.deltaTime);

        GenerateZoom();
        GenerateChromaticAbberation();
        GenerateVignette();

        transform.localPosition = currentPosition + GenerateCameraShake();
        transform.localRotation = Quaternion.Euler(rotation);
    }

    private void GenerateZoom()
    {
        if (attachedCamera)
            attachedCamera.orthographicSize = Mathf.Lerp(minZoom, maxZoom, currentZoom);
    }

    private Vector3 GenerateCameraShake()
    {
        return Random.insideUnitSphere * currentCameraShake * maxCameraShake;
    }

    private void GenerateChromaticAbberation()
    {
        if (chromaticAberration)
            chromaticAberration.intensity.value = currentChromaticAbberation * maxChromaticAbberation;
    }

    private void GenerateVignette()
    {
        if (vignette)
            vignette.intensity.value = Mathf.Lerp(minVignette, maxVignette, currentVignette);
    }

    public void ResetCamera()
    {
        currentPosition = TargetPosition;
    }

}
