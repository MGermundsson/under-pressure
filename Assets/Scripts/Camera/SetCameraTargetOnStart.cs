using UnityEngine;

/// <summary>
///     <para>Sets this object's transform as the camera's target.</para>
///     
///     <para>Destroys itself afterwards.</para>
/// </summary>
public class SetCameraTargetOnStart : MonoBehaviour
{
    [SerializeField] private bool resetCamera = true;

    void Start()
    {
        CameraController cameraController = FindObjectOfType<CameraController>();

        if (cameraController)
        {
            cameraController.Target = transform;

            if (resetCamera)
                cameraController.ResetCamera();
        }

        Destroy(this);
    }
}
