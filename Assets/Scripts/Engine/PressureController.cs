using UnityEngine;

public class PressureController : MonoBehaviour
{
    #region Singleton pattern

    private static PressureController instance;

    public static PressureController Instance
    {
        get
        {
            if (!instance)
            {
                GameObject gameObject = new GameObject("Pressure Controller");

                instance = gameObject.AddComponent<PressureController>();
            }

            return instance;
        }
    }

    #endregion


    [SerializeField] private float smoothing = 0.5f;
    

    private Room[] rooms;
    private Door[] doors;


    private void Start()
    {
        #region Singleton sanity-check

        if (instance)
        {
            Destroy(this);
            return;
        }

        instance = this;

        #endregion

        rooms = FindObjectsOfType<Room>();
        doors = FindObjectsOfType<Door>();
    }

    private void Update()
    {
        foreach (Door door in doors)
        {
            if (door.Open)
            {
                float averagePressure = (door.ConnectedRoom1.Pressure + door.ConnectedRoom2.Pressure) / 2f;

                door.ConnectedRoom1.Pressure = Mathf.Lerp(door.ConnectedRoom1.Pressure, averagePressure, 1 / smoothing * Time.deltaTime);
                door.ConnectedRoom2.Pressure = Mathf.Lerp(door.ConnectedRoom2.Pressure, averagePressure, 1 / smoothing * Time.deltaTime);
            }
        }
    }
}
