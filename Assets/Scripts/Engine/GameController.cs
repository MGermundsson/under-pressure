using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region Singleton pattern

    private static GameController instance;

    public static GameController Instance
    {
        get
        {
            if (!instance)
            {
                GameObject gameObject = new GameObject("Game Controller");

                instance = gameObject.AddComponent<GameController>();
            }

            return instance;
        }
    }

    #endregion


    [SerializeField] private PlayerHealth playerHealth;

    [Header("Progression Settings")]

    [SerializeField] private Lever electricityLever;
    [SerializeField] private Lever engineLever;

    [Header("Fade Settings")]

    [SerializeField] private CanvasGroup canvasGroup;

    [Space(5f)]

    [SerializeField] private AudioMixer audioMixer;

    [Space(5f)]

    [SerializeField] private float fadeSpeed = 1f;


    private float currentAlpha = 1f;

    private bool escaped = false;


    public bool ElectricityOn { get { return electricityLever.State; } }
    public bool EngineOn { get { return engineLever.State; } }


    private void OnValidate()
    {
        if (fadeSpeed < 0f)
            fadeSpeed = 0f;
    }
    private void Start()
    {
        #region Singleton sanity-check

        if (instance)
        {
            Destroy(this);
            return;
        }

        instance = this;

        #endregion 
    }

    private void Update()
    {
        float targetAlpha = (!playerHealth.Alive || escaped) ? 1f : 0f;

        currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, fadeSpeed * Time.deltaTime);

        if (canvasGroup)
            canvasGroup.alpha = currentAlpha;

        if (audioMixer)
            audioMixer.SetFloat("Master", Mathf.Log10(1f - currentAlpha) * 20f);

        if (currentAlpha > 0.999f)
        {
            if (!playerHealth.Alive)
                Restart();
            else if (escaped)
                SceneManager.LoadScene("Credits");
        }
    }

    private void Restart()
    {
        SceneManager.LoadScene("Main");
    }

    public void Win()
    {
        escaped = true;
    }
}
