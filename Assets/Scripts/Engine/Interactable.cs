using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public abstract class Interactable : MonoBehaviour
{
    [SerializeField] protected string title = "Interact";


    public string Title { get { return title; } }


    public abstract void Interact();
}
