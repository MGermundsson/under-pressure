using System.Collections.Generic;
using UnityEngine;

public class RoomDetector : MonoBehaviour
{
    [SerializeField] private float radius = 0.5f;


    private LayerMask layerMask;

    private Room currentRoom;


    public Room CurrentRoom { get { return currentRoom; } }


    private void Start()
    {
        layerMask = 1 << LayerMask.NameToLayer("Room");
    }

    private void Update()
    {
        currentRoom = GetClosestRoom();
    }

    private List<Room> GetRooms()
    {
        List<Room> rooms = new List<Room>();

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layerMask);
        foreach (Collider collider in colliders)
        {
            foreach (Room room in collider.GetComponentsInChildren<Room>())
            {
                rooms.Add(room);
            }

        }

        return rooms;
    }

    private Room GetClosestRoom()
    {
        Room closestRoom = null;

        List<Room> rooms = GetRooms();
        if (rooms.Count > 0)
        {
            closestRoom = rooms[0];

            if (rooms.Count > 1)
            {
                float smallestDistance = Vector3.Distance(transform.position, closestRoom.transform.position);

                for (int i = 1; i < rooms.Count; i++)
                {
                    float distance = Vector3.Distance(transform.position, rooms[i].transform.position);

                    if (distance < smallestDistance)
                    {
                        closestRoom = rooms[i];
                        smallestDistance = distance;
                    }
                }
            }
        }

        return closestRoom;
    }
}
