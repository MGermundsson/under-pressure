using TMPro;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private GameObject canvas;

    [Space(5f)]

    [SerializeField] private TMP_Dropdown dropdown;


    private bool ready = false;

    private bool open = false;


    private void Start()
    {
        int currentResolution = 0;

        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Resolution resolution = Screen.resolutions[i];

            dropdown.options.Add(new TMP_Dropdown.OptionData(resolution.ToString()));

            if (resolution.Equals(Screen.currentResolution))
                currentResolution = i;
        }

        dropdown.value = currentResolution;

        ready = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
            open = !open;

        canvas.SetActive(open);
    }

    public void SetQualityLevel(int index)
    {
        if (!ready)
            return;

        QualitySettings.SetQualityLevel(index);
    }

    public void SetFullScreenMode(int index)
    {
        if (!ready)
            return;

        if (index < 0 || index > 4)
            return;

        Screen.fullScreenMode = (FullScreenMode) index;
    }

    public void UpdateResolution()
    {
        if (!ready)
            return;

        int index = dropdown.value;

        if (index < 0 || index > Screen.resolutions.Length)
            return;

        Resolution resolution = Screen.resolutions[index];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreenMode);
    }

    public void Exit()
    {
        if (!ready)
            return;

        Debug.Log("Application Quit");
        Application.Quit();
    }
}
