using UnityEngine;

public class Rotater : MonoBehaviour
{
    [SerializeField] private Vector3 amount;

    private void Update()
    {
        transform.Rotate(amount * Time.deltaTime);
    }
}
