using UnityEngine;

/// <summary>
///     <para>Utility MonoBehaviour that destroys all mesh components on start.</para>
///     
///     <para>Useful for showing invisible walls in the editor.</para>
///     
///     <para>Affects the following components:</para>
///     
///     <list type="bullet">
///     <item>MeshFilter</item>
///         <item>MeshRenderer</item>
///     </list>
///     
///     <para>Also destroys itself afterwards.</para>
///     
///     <para>(Affects root object and all child objects)</para>
/// </summary>
public class DestroyMeshOnStart : MonoBehaviour
{
    void Start()
    {
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();

        foreach (MeshFilter meshFilter in meshFilters)
        {
            Destroy(meshFilter);
        }

        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            Destroy(meshRenderer);
        }

        Destroy(this);
    }
}
