using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] private bool moving = false;

    [SerializeField] private Vector3 velocity = Vector3.zero;


    private void Update()
    {
        if (moving)
            transform.Translate(velocity * Time.deltaTime);
    }
}
