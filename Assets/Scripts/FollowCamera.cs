using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Vector3 offset;


    private void LateUpdate()
    {
        transform.position = CameraController.Instance.transform.position + offset;
    }
}
