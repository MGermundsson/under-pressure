using UnityEngine;

[RequireComponent(typeof(Door))]
public class DoorAnimator : MonoBehaviour
{
    [SerializeField] private Transform leftDoor;
    [SerializeField] private Transform rightDoor;

    [Space(5f)]

    [SerializeField] private float offset = 1.4f;

    [Space(5f)]

    [SerializeField] [Range(0f, 1f)] private float smoothing = 0.2f;


    private Door door;


    private float state = 0f;


    private void Start()
    {
        door = GetComponent<Door>();
    }

    private void Update()
    {
        float targetState = door.Open ? 1f : 0f;

        state = Mathf.Lerp(state, targetState, 1 / smoothing * Time.deltaTime);

        leftDoor.localPosition = Vector3.left * offset * state;
        rightDoor.localPosition = Vector3.right * offset * state;
    }
}
