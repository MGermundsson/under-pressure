using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

[RequireComponent(typeof(AudioSource))]
public class PressureSource : MonoBehaviour
{
    [SerializeField] private Room connectedRoom;

    [Space(5f)]

    [SerializeField] [Range(0f, 10f)] private float targetPressure = 1f;
    [SerializeField] private float flowRate = 0.5f;

    [Space(5f)]

    [SerializeField] private bool canAddPressure = true;
    [SerializeField] private bool canRemovePressure = true;

    [Header("Effects Settings")]

    [SerializeField] private ParticleSystem attachedParticleSystem;

    [Space(5f)]

    [SerializeField] private float maxVolume = 0.05f;

    [Space(5f)]

    [SerializeField] private float pressureMin = 0f;
    [SerializeField] private float pressureMax = 1f;


    private AudioSource audioSource;

    private EmissionModule emissionModule;

    private float originalEmissionConstant;


    private void OnValidate()
    {
        if (flowRate < 0f)
            flowRate = 0f;
    }

    private void Start()
    {
        if (!connectedRoom)
            Debug.LogError("Room has not been set");

        audioSource = GetComponent<AudioSource>();

        emissionModule = attachedParticleSystem.emission;
        originalEmissionConstant = emissionModule.rateOverTime.constant;
    }

    private void Update()
    {
        UpdateRoom();
        UpdateAudio();
        UpdateParticleSystem();
    }

    private void UpdateRoom()
    {
        if (connectedRoom)
        {
            float currentPressure = connectedRoom.Pressure;

            // Abort if not a pressure source
            if (currentPressure < targetPressure && !canAddPressure)
                return;

            // Abort if not a pressure drain
            if (currentPressure > targetPressure && !canRemovePressure)
                return;

            connectedRoom.Pressure = Mathf.Lerp(currentPressure, targetPressure, flowRate * Time.deltaTime);
        }
    }

    private void UpdateAudio()
    {
        audioSource.volume = Mathf.Lerp(0f, maxVolume, GetEffectStrength());
    }

    private void UpdateParticleSystem()
    {
        if (attachedParticleSystem)
            emissionModule.rateOverTime = Mathf.Lerp(0f, originalEmissionConstant, GetEffectStrength());
    }

    private float GetEffectStrength()
    {
        float effectStrength = 0f;

        if (connectedRoom)
        {
            float currentPressure = connectedRoom.Pressure;

            effectStrength = Mathf.InverseLerp(pressureMin, pressureMax, currentPressure);

            effectStrength = Mathf.Clamp(effectStrength, 0f, currentPressure);
        }

        return effectStrength;
    }
}
