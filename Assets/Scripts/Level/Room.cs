using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Room : MonoBehaviour
{
    [SerializeField] private string title = "Room";

    [Space(5f)]

    [SerializeField] private float pressure = 1f;


    private BoxCollider[] boxColliders;


    public string Title { get { return title; } }
    public float Pressure { get { return pressure; } set { pressure = value; } }


    private void Start()
    {
        boxColliders = GetComponents<BoxCollider>();
    }

    private void Update()
    {
        pressure = Mathf.Clamp(pressure, 0f, 10f);
    }

    private void OnDrawGizmosSelected()
    {
        BoxCollider[] temporaryBoxColliders = GetComponents<BoxCollider>();

        foreach (BoxCollider boxCollider in temporaryBoxColliders)
        {
            Gizmos.color = new Color(0f, 0f, 1f, 0.25f);

            Gizmos.DrawCube(transform.position + boxCollider.center, boxCollider.size);
        }
    }
}
