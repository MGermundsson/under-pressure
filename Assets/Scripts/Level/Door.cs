using UnityEngine;

public class Door : Interactable
{
    [SerializeField] private bool open = false;

    [Space(5f)]

    [SerializeField] private Room connectedRoom1;
    [SerializeField] private Room connectedRoom2;

    [Space(5f)]

    [SerializeField] private Collider doorCollider;

    [Header("Sound Settings")]

    [SerializeField] private AudioSource oneShotSource;
    [SerializeField] private AudioSource loopSource;

    [Space(5f)]

    [SerializeField] private AudioClip interactionSound;
    [SerializeField] private AudioClip airStartSound;
    [SerializeField] private AudioClip airLoopSound;

    [Space(5f)]

    [SerializeField] private float airLoopVolume = 0.1f;

    [Space(5f)]

    [SerializeField] private float pressureDeltaMin = 0.2f;
    [SerializeField] private float pressureDeltaMax = 0.5f;


    public bool Open { get { return open; } set { open = value; } }

    public Room ConnectedRoom1 { get { return connectedRoom1; } }
    public Room ConnectedRoom2 { get { return connectedRoom2; } }

    public float PressureDelta
    {
        get
        {
            if (connectedRoom1 && connectedRoom2)
                return Mathf.Abs(connectedRoom1.Pressure - connectedRoom2.Pressure);
            else
                return 0f;
        }
    }


    private void Start()
    {
        loopSource.clip = airLoopSound;
        loopSource.Play();
        loopSource.time = Random.Range(0f, airLoopSound.length);

        if (!doorCollider)
            Debug.LogError("No door collider has been set");

        if (!connectedRoom1)
            Debug.LogError("Room 1 has not been set");

        if (!connectedRoom2)
            Debug.LogError("Room 2 has not been set");
    }

    private void Update()
    {
        if (doorCollider)
            doorCollider.enabled = !open;

        if (open)
            loopSource.volume = Mathf.InverseLerp(pressureDeltaMin, pressureDeltaMax, PressureDelta) * airLoopVolume;
        else
            loopSource.volume = 0f;
    }

    public override void Interact()
    {
        open = !open;

        oneShotSource.PlayOneShot(interactionSound);

        if (open && PressureDelta > pressureDeltaMin)
            oneShotSource.PlayOneShot(airStartSound);
    }
}
