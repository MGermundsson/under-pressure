public class Terminal : Interactable
{
    public override void Interact()
    {
        if (GameController.Instance.ElectricityOn && GameController.Instance.EngineOn)
        {
            GameController.Instance.Win();

            Destroy(this);
        }
    }

    private void Update()
    {
        if (!GameController.Instance.ElectricityOn)
            title = "There's no power...";
        else if (!GameController.Instance.EngineOn)
            title = "The engine isn't on...";
        else
            title = "Escape";
    }
}
