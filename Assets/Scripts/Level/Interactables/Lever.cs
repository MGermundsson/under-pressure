using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Lever : Interactable
{
    [SerializeField] private bool state = false;

    [Space(5f)]

    [SerializeField] private GameObject[] enabledWhenTurnedOff;
    [SerializeField] private GameObject[] enabledWhenTurnedOn;

    [Header("Audio Settings")]

    [SerializeField] private AudioClip interactAudioClip;

    [Header("Animation Settings")]

    [SerializeField] private Transform handleRoot;

    [Space(5f)]

    [SerializeField] private Vector3 offEulerAngles = Vector3.zero;
    [SerializeField] private Vector3 onEulerAngles = Vector3.zero;

    [Space(5f)]

    [SerializeField] private float smoothing = 0.5f;


    private AudioSource audioSource;


    public bool State { get { return state; } }


    private void OnValidate()
    {
        if (smoothing < 0f)
            smoothing = 0f;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        UpdateLinkedGameObjects();
    }

    private void Update()
    {
        Vector3 targetEulerAngles = state ? onEulerAngles : offEulerAngles;

        if (handleRoot)
            handleRoot.localRotation = Quaternion.Slerp(handleRoot.localRotation, Quaternion.Euler(targetEulerAngles), smoothing * Time.deltaTime);
    }

    public override void Interact()
    {
        state = !state;

        audioSource.PlayOneShot(interactAudioClip);

        UpdateLinkedGameObjects();
    }

    private void UpdateLinkedGameObjects()
    {
        foreach (GameObject o in enabledWhenTurnedOff)
        {
            o.SetActive(!state);
        }

        foreach (GameObject o in enabledWhenTurnedOn)
        {
            o.SetActive(state);
        }
    }
}
