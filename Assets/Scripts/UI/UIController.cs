using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI roomTitleText;
    [SerializeField] private TextMeshProUGUI pressureValueText;

    [Space(5f)]

    [SerializeField] private TextMeshProUGUI interactableTitleText;
    [SerializeField] private Vector3 interactableTitleOffset;

    [Space(5f)]

    [SerializeField] private RectTransform healthBar;

    [Space(5f)]

    [SerializeField] private float healthBarWidth;

    [Space(5f)]

    [SerializeField] private Transform playerTransform;
    [SerializeField] private RoomDetector roomDetector;
    [SerializeField] private PlayerHealth playerHealth;
    [SerializeField] private PlayerInteractionController playerInteractionController;


    private void Update()
    {
        if (playerHealth)
        {
            healthBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, healthBarWidth * playerHealth.CurrentHealthPercentage);
        }

        if (roomDetector)
        {
            Room currentRoom = roomDetector.CurrentRoom;

            if (currentRoom)
            {
                roomTitleText.text = currentRoom.Title;
                pressureValueText.text = string.Format("{0:0.00} atm", currentRoom.Pressure);
            }
        }

        if (playerInteractionController)
        {
            Interactable currentInteractable = playerInteractionController.CurrentInteractable;

            if (currentInteractable)
            {
                interactableTitleText.text = currentInteractable.Title;
            }
            else
            {
                interactableTitleText.text = "";
            }
        }

        if (playerTransform)
        {
            Vector3 playerScreenPosition = CameraController.Instance.AttachedCamera.WorldToScreenPoint(playerTransform.position + interactableTitleOffset);

            interactableTitleText.rectTransform.position = playerScreenPosition;
        }
    }
}
