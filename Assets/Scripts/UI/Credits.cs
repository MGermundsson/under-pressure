using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    [SerializeField] private Transform creditsRoot;
    [SerializeField] private Vector3 creditsVelocity = Vector3.up;

    [Space(5f)]

    [SerializeField] private string nextSceneName;
    [SerializeField] private float timer = 10f;


    void Update()
    {
        if (creditsRoot)
            creditsRoot.Translate(creditsVelocity * Time.deltaTime);

        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            if (nextSceneName != "")
                SceneManager.LoadScene(nextSceneName);
            else
            {
                Debug.Log("Application Quit");
                Application.Quit();
            }
        }
    }
}
